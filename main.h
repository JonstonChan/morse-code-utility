#ifndef MAIN_H
#define MAIN_H

typedef enum mode {ENCODE, DECODE} Mode;

void displayHelp();

void displayInvalidStreamError(const char* streamName);

#endif