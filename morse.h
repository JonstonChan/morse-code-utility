#ifndef MORSE_H
#define MORSE_H

#include <limits.h>
#include <stdbool.h>

#define MAX_MORSE_CODE_LENGTH 10
#define SYMBOLS_LENGTH (UCHAR_MAX + 1)
#define UNDEFINED_SYMBOL '\0'
#define DEFAULT_SYMBOLS_FILE "symbols.txt"

char SYMBOLS[SYMBOLS_LENGTH][MAX_MORSE_CODE_LENGTH];

bool loadSymbols(const char *symbolsFilename);

char *getRepresentation(unsigned char symbol);

char getSymbol(char *representation);

#endif