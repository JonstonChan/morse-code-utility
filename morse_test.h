#ifndef MORSE_TEST_H
#define MORSE_TEST_H

void testLoadSymbols();
void testAlphabetic();
void testNumeric();
void testWhitespace();
void testNonAlphanumeric();
void testUndefinedAscii();
void testMultibyte();

#endif