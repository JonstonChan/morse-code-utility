#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "morse_test.h"
#include "morse.h"

int main() {
	testLoadSymbols();

	loadSymbols(DEFAULT_SYMBOLS_FILE);

	testAlphabetic();
	testNumeric();
	testWhitespace();
	testNonAlphanumeric();
	testUndefinedAscii();
	testMultibyte();

	puts("All tests passed");

	return 0;
}


void testLoadSymbols() {
	{
		// It is not possible to have a file with an empty name
		bool symbolsLoaded = loadSymbols("");
		assert(symbolsLoaded == false);
	}

	{
		FILE *filePointer = fopen(DEFAULT_SYMBOLS_FILE, "r");

		if (filePointer) {
			bool symbolsLoaded = loadSymbols(DEFAULT_SYMBOLS_FILE);
			assert(symbolsLoaded == true);
			fclose(filePointer);
		} else {
			puts("Default symbols file must exist to continue testing.");
			exit(EXIT_FAILURE);
		}
	}
}


void testAlphabetic() {
	{
		char *representation = getRepresentation('a');
		assert(strcmp(representation, ".-") == 0);
		assert(getSymbol(representation) == 'A');
		free(representation);
	}

	{
		char *representation = getRepresentation('A');
		assert(strcmp(representation, ".-") == 0);
		assert(getSymbol(representation) == 'A');
		free(representation);
	}
}


void testNumeric() {
	{
		char *representation = getRepresentation('0');
		assert(strcmp(representation, "-----") == 0);
		assert(getSymbol(representation) == '0');
		free(representation);
	}

	{
		char *representation = getRepresentation('3');
		assert(strcmp(representation, "...--") == 0);
		assert(getSymbol(representation) == '3');
		free(representation);
	}

	{
		char *representation = getRepresentation('5');
		assert(strcmp(representation, ".....") == 0);
		assert(getSymbol(representation) == '5');
		free(representation);
	}
}


void testWhitespace() {
	{
		char *representation = getRepresentation(' ');
		assert(strcmp(representation, "/") == 0);
		assert(getSymbol(representation) == ' ');
		free(representation);
	}

	{
		char *representation = getRepresentation('\n');
		assert(strcmp(representation, "//") == 0);
		assert(getSymbol(representation) == '\n');
		free(representation);
	}

	{
		char *representation = getRepresentation('\t');
		assert(strcmp(representation, "///") == 0);
		assert(getSymbol(representation) == '\t');
		free(representation);
	}
}


void testNonAlphanumeric() {
	{
		char *representation = getRepresentation('@');
		assert(strcmp(representation, ".--.-.") == 0);
		assert(getSymbol(representation) == '@');
		free(representation);
	}

	{
		char *representation = getRepresentation('+');
		assert(strcmp(representation, ".-.-.") == 0);
		assert(getSymbol(representation) == '+');
		free(representation);
	}
}


void testUndefinedAscii() {
	{
		char *representation = getRepresentation('{');
		assert(strcmp(representation, "{") == 0);
		assert(getSymbol(representation) == '{');
		free(representation);
	}

	{
		char *representation = getRepresentation(';');
		assert(strcmp(representation, ";") == 0);
		assert(getSymbol(representation) == ';');
		free(representation);
	}
}


// Multibyte characters may appear in user input,
// but not from getRepresentation()
// Therefore, only getSymbol() needs to be tested
void testMultibyte() {
	{
		assert(getSymbol("☺") == UNDEFINED_SYMBOL);
	}
}