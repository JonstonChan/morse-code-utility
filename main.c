#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "morse.h"

#define STR_VALUE(x) STR(x)
#define STR(x) #x

int main(int argc, char *argv[]) {
	Mode mode = ENCODE;
	bool symbolsLoaded = false;
	char *outputFilename = NULL;

	// Inhibit "unrecognized option" messages
	opterr = 0;

	struct option long_options[] = {
		{"help",   no_argument,       NULL, 'h'},
		{"encode", no_argument,       NULL, 'e'},
		{"decode", no_argument,       NULL, 'd'},
		{"output", required_argument, NULL, 'o'},
		{"symbol", required_argument, NULL, 's'},
		{0, 0, 0, 0}
	};

	int option_index = 0;

	int c;

	while ((c = getopt_long(argc, argv, "hedo:s:", long_options, &option_index)) != -1) {
		switch (c) {
			case 'h': {
				displayHelp();
				exit(EXIT_SUCCESS);
				break;
			}

			case 'e': {
				mode = ENCODE;
				break;
			}

			case 'd': {
				mode = DECODE;
				break;
			}

			case 'o': {
				outputFilename = strdup(optarg);
				break;
			}

			case 's': {
				char *symbolsFilename = strdup(optarg);
				symbolsLoaded = loadSymbols(symbolsFilename);

				if (!symbolsLoaded) {
					displayInvalidStreamError(symbolsFilename);
					free(symbolsFilename);
					exit(EXIT_FAILURE);
				}

				free(symbolsFilename);
				break;
			}

			case '?': {
				puts(argv[optind - 1]);
				putchar('\n');
				displayHelp();
				exit(EXIT_FAILURE);
				break;
			}
		}
	}

	// No input filename provided
	if (optind >= argc) {
		puts("Missing input file.\n");
		displayHelp();
		exit(EXIT_FAILURE);
	}

	if (!symbolsLoaded) {
		symbolsLoaded = loadSymbols(DEFAULT_SYMBOLS_FILE);

		if (!symbolsLoaded) {
			displayInvalidStreamError(DEFAULT_SYMBOLS_FILE);
			exit(EXIT_FAILURE);
		}
	}

	char *inputFilename = argv[optind++];
	FILE *inputStream = fopen(inputFilename, "r");

	if (!inputStream) {
		displayInvalidStreamError(inputFilename);
		exit(EXIT_FAILURE);
	}


	FILE *outputStream;

	if (outputFilename) {
		if (strcmp(inputFilename, outputFilename) == 0) {
			puts("Input and output file must be different.");
			exit(EXIT_FAILURE);
		}

		outputStream = fopen(outputFilename, "w");

		if (!outputStream) {
			displayInvalidStreamError(outputFilename);
			free(outputFilename);
			exit(EXIT_FAILURE);
		}
	} else {
		outputStream = stdout;
	}

	if (mode == ENCODE) {
		char symbol;
		while ((symbol = fgetc(inputStream)) != EOF) {
			char *representation = getRepresentation(symbol);

			if ((symbol = fgetc(inputStream)) != EOF) {
				fprintf(outputStream, "%s ", representation);
			} else {
				fputs(representation, outputStream);
			}

			ungetc(symbol, inputStream);

			free(representation);
		};
	} else if (mode == DECODE) {
		char representation[MAX_MORSE_CODE_LENGTH];
		while (fscanf(inputStream, "%" STR_VALUE(MAX_MORSE_CODE_LENGTH) "s", representation) == 1) {
			fputc(getSymbol(representation), outputStream);
		}
	}

	if (outputFilename) {
		free(outputFilename);
	}

	if (inputStream != stdin) {
		fclose(inputStream);
	}

	if (outputStream != stdout) {
		fclose(outputStream);
	}

	return 0;
}


void displayHelp() {
	puts(
		"Usage: ./morse [OPTIONS] [FILE]\n"
		"\n"
		"Mandatory arguments to long options are mandatory for short options too.\n"
		"  -h, --help                  display this help and exit\n"
		"  -e, --encode                encode message (default)\n"
		"  -d, --decode                decode message\n"
		"  -o, --output FILE           write output to FILE; default: stdout\n"
		"  -s, --symbol FILE           read symbols from FILE; default: symbols.txt"
	);
}


void displayInvalidStreamError(const char* streamName) {
	size_t messageLength = strlen("Error opening \"") + strlen(streamName) + strlen("\"") + 1;
	char *message = malloc(messageLength);

	if (!message) {
		perror("Call to malloc failed");
		exit(EXIT_FAILURE);
	}

	strcpy(message, "Error opening \"");
	strcat(message, streamName);
	strcat(message, "\"");

	perror(message);

	free(message);
}