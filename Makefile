.PHONY: all clean install uninstall

EXECUTABLE=morse
EXECUTABLE_TEST=$(EXECUTABLE)_test

PREFIX=.

CCFLAGS=-g -Wall -Werror
CC=gcc

all: $(EXECUTABLE) $(EXECUTABLE_TEST)

debug: CCFLAGS += --coverage
debug: all

%.o: %.c
	$(CC) $(CCFLAGS) -c $^

$(EXECUTABLE): main.o morse.o
	$(CC) -o $(EXECUTABLE) $(CCFLAGS) $^

$(EXECUTABLE_TEST): morse_test.o morse.o
	$(CC) -o $(EXECUTABLE_TEST) $(CCFLAGS) $^

clean:
	rm -f *.o $(EXECUTABLE) $(EXECUTABLE_TEST)
	rm -f *.gcov *.gcda *.gcno

install: all
	mkdir -p $(PREFIX)/bin
	cp morse $(PREFIX)/bin
	cp symbols.txt $(PREFIX)/bin

uninstall:
	rm -rf $(PREFIX)/bin