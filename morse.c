#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "morse.h"

bool loadSymbols(const char *symbolsFilename) {
	FILE *filePointer = fopen(symbolsFilename, "r");

	if (!filePointer) {
		return false;
	}

	// Initialize all symbols to be the same as representation
	// For example, '{' will remain a '{' when encoded unless
	// defined in the symbols file
	for (int i = 0; i < SYMBOLS_LENGTH; i++) {
		SYMBOLS[i][0] = i;
		SYMBOLS[i][1] = '\0';
	}

	unsigned char symbol;
	char representation[MAX_MORSE_CODE_LENGTH];

	while (fscanf(filePointer, "%c %s ", &symbol, representation) == 2) {
		symbol = toupper(symbol);
		strcpy(SYMBOLS[symbol], representation);
	}

	strcpy(SYMBOLS[' '], "/");
	strcpy(SYMBOLS['\n'], "//");
	strcpy(SYMBOLS['\t'], "///");

	fclose(filePointer);

	return true;
}


char *getRepresentation(unsigned char symbol) {
	symbol = toupper(symbol);
	return strdup(SYMBOLS[symbol]);
}


char getSymbol(char *representation) {
	for (int i = 0; i < UCHAR_MAX + 1; i++) {
		if (strcmp(SYMBOLS[i], representation) == 0) {
			return i;
		}
	}

	// Multibyte character
	return UNDEFINED_SYMBOL;
}