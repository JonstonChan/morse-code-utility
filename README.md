Morse code utility
===


Description
---

Morse code utility is a small C program that encodes and decodes Morse code


Usage
---

```sh
# Build executable
make

# Show help
./morse --help

# Create input file
echo 'Hello, World!' > 'input.txt'

# Encode contents and save to file
./morse 'input.txt' > 'output.txt'

# Display encoded contents
cat 'output.txt'

# Delete original input
rm 'input.txt'

# Decode encoded contents and display
./morse --decode 'output.txt'
```


Warnings
---

Case is not preserved. For example, encoding and decoding "Hello" will result in "HELLO". This is consistent with how Morse code is used in telecommunications.